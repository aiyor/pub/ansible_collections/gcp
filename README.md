# Ansible Collection - aiyor.gcp

This is a repository for hosting miscallaneous GCP related Ansible Collection.

Some of the plugins (i.e., modules, lookups, filters) here may be contributed back to https://github.com/ansible-collections/google.cloud

## Documenetation

Each plugin has self-contained documentation in the code which can be generated as Ansible document.

The self-contained documents in each of the plugin should be self-explanatory with information on input and output parameters, in-line with the official Ansible module documents.

## License

Generally, all codes in the repository are licensed as GPL-3 by default.  Each plugin should include license reference.

## Plugin List

### Modules

#### `gcp_resourcemanager_folders_info`:

This module helps retrieve GCP Folders by either listing every folder under and `Organization`, or by searching a folder based on specific query.

#### `gcp_secretmanager_info`:

This module gather facts of the GCP Secret Manager in a `Project`.  This does not retrieve the actual secret payload, it only retrieve the metadata.  For retrieving secret payload, refer to `gcp_secret` lookup.


### Lookup

#### `gcp_accesstoken`:

This Lookup will generate an Oauth2 access token based on the GCP credential/service account provided.

#### `gcp_secret`:

This Lookup retrieve a secret payload from a given secret stored in GCP Secret Manager.

#### `gcp_secrets`:

This Lookup retrieve list of secret names exist in a GCP Secret Manager.
