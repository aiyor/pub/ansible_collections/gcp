# (c) 2023, Tze L. <https://gitlab.com/tze>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

# References:
#   - API:
#       - https://google-auth.readthedocs.io/en/master/reference/google.auth.html#google.auth.default
#       - https://developers.google.com/identity/protocols/oauth2/javascript-implicit-flow#obtainingaccesstokens
#   - Lookup workflow based on: https://github.com/ansible-collections/community.google/blob/main/plugins/lookup/gcp_storage_file.py

from __future__ import absolute_import, division, print_function
import os
import requests
import json
import jwt
import time
import google.auth
import google.auth.compute_engine
import google.oauth2
import google.auth.transport.requests
from ansible.plugins.lookup import LookupBase
from ansible.utils.display import Display

__metaclass__ = type

DOCUMENTATION = """
lookup: gcp_accesstoken
description:
  - This lookup returns the GCP OAuth2 access token from either service account key,
    service account via workload identity federation, or service account attached
    to Google VM.
requirements:
  - python >= 3.0
  - requests >= 2.18.4
  - google-auth >= 1.3.0
options:
  auth_kind:
    description:
    - The type of credential used.
    type: str
    default: environment variable 'GCP_AUTH_KIND'
    required: true
    choices:
    - application
    - machineaccount
    - serviceaccount
  service_account_contents:
    description:
    - The contents of a Service Account JSON file, either in a dictionary or as a
      JSON string that represents it.
    type: jsonarg
  service_account_file:
    description:
    - The path of a Service Account JSON file if serviceaccount is selected as type.
    type: path
  service_account_email:
    description:
    - An optional service account email address if machineaccount is selected and
      the user does not wish to use the default email.
    type: str
notes:
- for authentication, you can set service_account_file using the C(GCP_SERVICE_ACCOUNT_FILE)
  env variable.
- for authentication, you can set service_account_contents using the C(GCP_SERVICE_ACCOUNT_CONTENTS)
  env variable.
- For authentication, you can set service_account_email using the C(GCP_SERVICE_ACCOUNT_EMAIL)
  env variable.
- For authentication, you can set access_token using the C(GCP_ACCESS_TOKEN)
  env variable.
- For authentication, you can set auth_kind using the C(GCP_AUTH_KIND) env variable.
- Environment variables values will only be used if the playbook values are not set.
- The I(service_account_email) and I(service_account_file) options are mutually exclusive.
"""

EXAMPLES = """
- ansible.builtin.debug: # Using service account json authentication file
    msg: |
         the google access token is {{
         lookup(
           'gcp_accesstoken',
           auth_kind='serviceaccount',
           service_account_file='/tmp/myserviceaccountfile.json')
         }}
"""

RETURN = """
_raw:
    description:
        - OAuth2 access token string
"""

display = Display()


class GcpAccessTokenLookup:
    def run(self, method, **kwargs):
        params = {
            "auth_kind": kwargs.get("auth_kind", os.environ.get("GCP_AUTH_KIND")),
            "service_account_file": kwargs.get("service_account_file", os.environ.get("GCP_SERVICE_ACCOUNT_FILE")),
            "service_account_email": kwargs.get("service_account_email", os.environ.get("GCP_SERVICE_ACCOUNT_EMAIL")),
            "service_account_contents": kwargs.get("service_account_contents", os.environ.get("GCP_SERVICE_ACCOUNT_CONTENTS")),
            "scope": "https://www.googleapis.com/auth/cloud-platform",
        }
        result = self.get_access_token(params)
        return [result]

    def get_access_token(self, params):
        if params['auth_kind'] == 'application':
            if params['service_account_file'] is None:
                credentials, project_id = google.auth.default(
                    scopes=params['scope'])
            else:
                credentials, project_id = google.auth.load_credentials_from_file(
                    scopes=params['scope'], filename=params['service_account_file'])
            auth_req = google.auth.transport.requests.Request()
            credentials.refresh(auth_req)
            return credentials.token
        if params['auth_kind'] == 'serviceaccount':
            if params['service_account_file'] is not None:
                path = os.path.realpath(os.path.expanduser(params['service_account_file']))
                with open(path, 'r') as f:
                    cred = json.load(f)
            elif params['service_account_contents'] is not None:
                try:
                    cred = json.loads(params['service_account_contents'])
                except json.decoder.JSONDecodeError as e:
                    print(
                        "Unable to decode service_account_contents as JSON: %s" % e
                    )
                else:
                    print(
                        'Service Account authentication requires setting either service_account_file or service_account_contents')
            return [self.get_token_from_service_account(service_account_content=cred, params=params)]

        if params['auth_kind'] == 'machineaccount':
            # email = self.module.params['service_account_email']
            # email = email if email is not None else "default"
            email = "default"
            credentials = google.auth.compute_engine.Credentials(email)
            auth_req = google.auth.transport.requests.Request()
            credentials.refresh(auth_req)
            return credentials.token

    def get_token_from_service_account(self, service_account_content, params):
        token_url = service_account_content['token_uri']
        private_key = service_account_content['private_key']
        payload = {
            'iss': service_account_content['client_email'],
            'scope': params['scope'],
            'aud': token_url,
            'exp': int(time.time()) + 3600,
            'iat': int(time.time())
        }
        assertion_token = jwt.encode(payload, private_key, algorithm='RS256')
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        data = {
            'grant_type': 'urn:ietf:params:oauth:grant-type:jwt-bearer',
            'assertion': assertion_token
        }
        response = requests.post(token_url, headers=headers, data=data)
        if response.status_code == 200:
            return response.json()['access_token']
        else:
            raise Exception('Failed to retrieve access token')


class LookupModule(LookupBase):
    def run(self, terms, variables=None, **kwargs):
        return GcpAccessTokenLookup().run(terms, variables=variables, **kwargs)
